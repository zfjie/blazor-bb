﻿using BootstrapBlazor.Components;

using BootstrapBlazorApp.OnlyServer.Data;

using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using System.Diagnostics.CodeAnalysis;

namespace BootstrapBlazorApp.OnlyServer.Pages
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TableDemo : ComponentBase
    {
        [Inject]
        [NotNull]
        private IStringLocalizer<Foo>? Localizer { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private static IEnumerable<int> PageItemsSource => new int[] { 20, 40 };

        Task<QueryData<Foo>> OnQueryAsync(QueryPageOptions options)
        {
            IEnumerable<Foo> items = Foo.GenerateFoo(Localizer);
            var total = items.Count();

            bool isSorted = false;
            // 此段代码可不写，组件内部自行处理
            if (options.SortName == nameof(Foo.DateTime) && options.SortList != null)
            {
                items = items.Sort(options.SortList);
                isSorted = true;
            }
            else if (!string.IsNullOrEmpty(options.SortName))
            {
                // 外部未进行排序，内部自动进行排序处理
                items = items.Sort(options.SortName, options.SortOrder);
                isSorted = true;
            }
            items = items.Skip((options.PageIndex - 1) * options.PageItems).Take(options.PageItems).ToList();

            QueryData<Foo> queryData = new QueryData<Foo>()
            {
                Items = items,
                IsSorted = isSorted,
                TotalCount = total
            };
            return Task.FromResult(queryData);
        }

    }
}