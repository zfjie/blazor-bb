﻿namespace BootstrapBlazorApp.OnlyServer.Pages.Main.Users
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public int Age { get; set; }

        public string Email { get; set; }

        public string Port { get; set; }

        public decimal Grt { get; set; }
    }
}
