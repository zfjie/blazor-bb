﻿using System.ComponentModel.DataAnnotations;

namespace BootstrapBlazorApp.OnlyServer.Pages.Main.Users
{
    public class UserCreateOrUpdateDto
    {
        [Required]
        public Guid Id { get; set; }

        [Display(Name = "姓名")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [StringLength(10, MinimumLength = 2, ErrorMessage = "{0} 长度在 2-10 个字符")]
        public string Name { get; set; }

        [Display(Name = "年龄")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [Range(18, 78, ErrorMessage = "{0} 在18到78之间")]
        public int Age { get; set; }

        [Display(Name = "邮箱")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [StringLength(35, MinimumLength = 6, ErrorMessage = "{0} 长度在 6-35 个字符")]
        public string Email { get; set; }

        [Display(Name = "港口")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "{0} 长度在 2-20 个字符")]
        public string Port { get; set; }
    }
}