﻿using BootstrapBlazorApp.OnlyServer.Data;
using Microsoft.AspNetCore.Mvc;

namespace BootstrapBlazorApp.OnlyServer.Collections
{
    public class TemplatesController : Controller
    {
        public IActionResult Index()
        {
            List<Student> students = new()
                {
                    new Student(){  Id=1,Name="Tom",Age=18,Address="浙江省宁波市鄞州区"},
                    new Student(){  Id=2,Name="流川枫",Age=18,Address="浙江省宁波市海曙区"},
                    new Student(){  Id=3,Name="樱木花道",Age=18,Address="浙江省宁波市江东区"},
                    new Student(){  Id=4,Name="Lucy",Age=18,Address="浙江省宁波市北仑区"},
                };

            return View(students);
        }
    }
}
