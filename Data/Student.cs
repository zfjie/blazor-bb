﻿using System.ComponentModel.DataAnnotations;

namespace BootstrapBlazorApp.OnlyServer.Data
{
    public class Student
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} 不可为空")]
        [Display(Name = "姓名")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "最多50个字符")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "{0} 不可为空")]
        [Display(Name = "年龄")]
        [Range(18, 40, ErrorMessage = "{0} 18到40之间")]
        public int Age { get; set; }

        [Required]
        [Display(Name = "地址")]
        public string? Address { get; set; }

        [Required(ErrorMessage = "{0} 不可为空")]
        [Display(Name = "正在学习中")]
        public bool? IsStudying { get; set; }

        //[Required(ErrorMessage ="{0} 不可为空")]
        //[Display(Name="是否中国人")]
        public bool? IsChina { get; set; }

        [Required(ErrorMessage = "{0} 不可为空")]
        [Display(Name = "国家")]
        public string? Country { get; set; }

        [Required(ErrorMessage = "{0} 不可为空")]
        [Display(Name = "省份")]
        public string? Province { get; set; }

        [Required(ErrorMessage = "{0} 不能为空")]
        [Display(Name = "ETC")]
        public DateTime? ETC { get; set; }

        public Address Study { get; set; } = new();
    }

    public class Address
    {
        [Display(Name = "学习地点")]
        [FixedLengthOrNull(8, ErrorMessage = "长度必须是8位")]
        public string StudyAddress { get; set; }
    }

    public class FixedLengthOrNullAttribute : ValidationAttribute
    {
        public FixedLengthOrNullAttribute(int fixedLength)
        {
            FixedLength = fixedLength;
        }
        public int FixedLength { get; private set; }
        public override bool IsValid(object value)
        {
            if (value != null)
            {
                string v = value.ToString();
                if (!string.IsNullOrEmpty(v))
                {
                    return v.Length == FixedLength;
                }
            }
            return true;
        }
    }
}
