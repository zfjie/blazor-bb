﻿using BootstrapBlazor.Components;

namespace BootstrapBlazorApp.OnlyServer.Data
{
    public class CheckBoxItem
    {
        public string? Text { get; set; }

        public string? Value { get; set; }

        public CheckboxState CheckboxState { get; set; } = CheckboxState.Indeterminate;
    }
}
