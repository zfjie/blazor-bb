﻿using System.ComponentModel.DataAnnotations;

namespace BootstrapBlazorApp.OnlyServer.Data
{
    public abstract class CompanyBaseDto
    {
        [Required(ErrorMessage = "{0} 不能为空")]
        [Display(Name = "姓名")]
        public virtual string Name { get; set; }

        public virtual string Address { get; set; }
    }

    public class NingboCompany : CompanyBaseDto
    {
        [Required(ErrorMessage = "{0} 不能为空")]
        [Display(Name = "地址")]
        public new string Address { get; set; }
    }

    public class ShanghaiCompany : CompanyBaseDto
    {
        public new string Name { get; set; }
    }
}
