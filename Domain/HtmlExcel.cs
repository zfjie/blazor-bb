﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RazorEngine;
using RazorEngine.Templating; // For extension methods.

namespace Common.Excel
{
    public class HtmlExcel
    {
        public static string ListToExcel<T>(List<T> dySource, string templateFileName)
        {
            var tk = Engine.Razor.GetKey(templateFileName);

            if (Engine.Razor.IsTemplateCached(tk, typeof(List<T>)))
            {
                string excelhtml = Engine.Razor.Run(tk, typeof(List<T>), dySource);
                return excelhtml;
                
            }
            else
            {
                string template = File.ReadAllText(templateFileName);//得到文件内容
                string excelhtml = Engine.Razor.RunCompile(template, templateFileName, typeof(List<T>), dySource);
                return excelhtml;
            }

        }


    }
}

