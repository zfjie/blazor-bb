﻿using BootstrapBlazorApp.OnlyServer.Data;

using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();

builder.Services.AddBootstrapBlazor();
builder.Services.AddBootstrapBlazorTableExportService();



builder.Services.AddSingleton<WeatherForecastService>();

//ServiceConfigurationContext



// 增加 Table 数据服务操作类
builder.Services.AddTableDemoDataService();

var app = builder.Build();



if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Error");
}

app.UseStaticFiles();

app.UseRouting();
app.MapBlazorHub();
app.MapFallbackToPage("~/main/{**segment}", "/Main/_Host");
app.MapFallbackToPage("~/templates/{**segment}", "/Templates/_Host");


app.MapControllers();

//app.MapFallbackToController()



app.Run();
