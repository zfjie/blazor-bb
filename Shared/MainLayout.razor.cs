﻿using BootstrapBlazor.Components;

using Microsoft.AspNetCore.Components.Routing;

namespace BootstrapBlazorApp.OnlyServer.Shared
{
    /// <summary>
    /// 
    /// </summary>
    public sealed partial class MainLayout
    {
        private bool UseTabSet { get; set; } = true;

        private string Theme { get; set; } = "";

        private bool IsOpen { get; set; }

        private bool IsFixedHeader { get; set; } = true;

        private bool IsFixedFooter { get; set; } = true;

        private bool IsFullSide { get; set; } = true;

        private bool ShowFooter { get; set; } = true;

        private List<MenuItem>? Menus { get; set; }

        /// <summary>
        /// OnInitialized 方法
        /// </summary>
        protected override void OnInitialized()
        {
            base.OnInitialized();

            Menus = GetIconSideMenuItems();
        }

        private static List<MenuItem> GetIconSideMenuItems()
        {
            var menus = new List<MenuItem>
        {
            new MenuItem() { Text = "返回组件库", Icon = "fa-solid fa-fw fa-home", Url = "https://www.blazor.zone/components" },
            new MenuItem() { Text = "Index", Icon = "fa-solid fa-fw fa-flag", Url = "/" , Match = NavLinkMatch.All},
            new MenuItem() { Text = "实现类验证", Icon = "fa-solid fa-fw fa-check-square", Url = "company" },
            new MenuItem() { Text = "用户", Icon = "fa-solid fa-fw fa-check-square", Url = "users1" },
            new MenuItem() { Text = "验证", Icon = "fa-solid fa-fw fa-check-square", Url = "/validate" },
            new MenuItem() { Text = "验证2", Icon = "fa-solid fa-fw fa-check-square", Url = "/validate2" },
            new MenuItem() { Text = "Tab", Icon = "fa-solid fa-fw fa-check-square", Url = "/tab1" },
            new MenuItem() { Text = "many", Icon = "fa-solid fa-fw fa-check-square", Url = "/many" },
            new MenuItem() { Text = "Counter", Icon = "fa-solid fa-fw fa-check-square", Url = "/counter" },
            new MenuItem() { Text = "FetchData", Icon = "fa-solid fa-fw fa-database", Url = "fetchdata" },
            new MenuItem() { Text = "Table", Icon = "fa-solid fa-fw fa-table", Url = "table" },
            new MenuItem() { Text = "花名册", Icon = "fa-solid fa-fw fa-users", Url = "users" },
            new MenuItem() { Text = "AutoFill", Icon = "fa-solid fa-fw fa-users", Url = "autofill" },
            new MenuItem() { Text = "File Preview", Icon = "fa-solid fa-fw fa-users", Url = "filepreview" },
            new MenuItem() { Text = "ListGroup", Icon = "fa-solid fa-fw fa-users", Url = "listgroup" },
            new MenuItem() { Text = "DDD", Icon = "fa-solid fa-fw fa-users", Url = "settings" },
            new MenuItem() { Text = "DynamicTable", Icon = "fa-solid fa-fw fa-users", Url = "table/dynamic" },
            new MenuItem() { Text = "DynamicTableObject", Icon = "fa-solid fa-fw fa-users", Url = "t2" },
            new MenuItem() { Text = "HtmlToExcel", Icon = "fa-solid fa-fw fa-users", Url = "htmltoexcel" },
            new MenuItem() { Text = "动态文本框", Icon = "fa-solid fa-fw fa-users", Url = "search" },
            new MenuItem() { Text = "动态文本框", Icon = "fa-soldi"}
        };

            return menus;
        }

      
    }
}