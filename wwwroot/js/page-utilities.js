﻿(function (win, undefined) {

    var obj = {

        setAutoComplete: function (id) {
            var showDropdown = function (t) {
                if ($(id).is('[readonly]')) {
                    if (!t.parent().find('.dropdown-menu').is(":hidden")) {
                        t.dropdown('toggle');
                    }
                    return;
                }
                if (t.parent().find('.dropdown-menu').is(":hidden")) {
                    t.dropdown('toggle');
                }
            };
            $(id).off('focus').on('focus', function () {
                $(id).select();
                if ($(id).is('[readonly]')) {
                    $(id).parent().find('.dropdown-menu').attr('hidden', true);
                }
                else {
                    $(id).parent().find('.dropdown-menu').removeAttr('hidden');
                }
            });
            $(id).off('input').on('input', function () { showDropdown($(this)); });
            $(id).off('keydown').on('keydown', function (event) {
                if (event.key === 'ArrowDown') {
                    $(this).parent().find('.dropdown-item').first().trigger('focus');
                    event.preventDefault();
                }
                if (event.which === 9) { // tab
                    $(id).parent().find('.dropdown-menu').attr('hidden', true);
                }
            });
        },

        validateForm: function (id) {
            var form = $(id).get(0);
            form.checkValidity();
            form.classList.add('was-validated');
        }

    };

    var obj2 = {
        setAutoComplete: function (id) {
            $(id).on('click', function () {
                $(id).select();
                $(id).parent().find('.dropdown-menu').removeAttr('hidden').css({ 'transform': 'translate(0px, 42px)' });
            });

            $(id).off('keydown').on('keydown', function (event) {
                if (event.key === 'ArrowDown') {
                    $(this).parent().find('.dropdown-item').first().trigger('focus');
                    event.preventDefault();
                }
                if (event.which === 9) { // tab
                    $(id).parent().find('.dropdown-menu').attr('hidden', true);
                }
            });
        },
    };

    var obj3 = {
        ctrlSelected: function (id) {
            var IsProfessing = false;
            $(document).keydown(function (e) {
                if (e.which === 17) { // 检测按下的键是否是Ctrl键
                    IsProfessing = true;
                }
            }).keyup(function (e) {
                if (e.which === 17) { // 检测释放的键是否是Ctrl键
                    IsProfessing = false;
                }
            });
            // 为表格的行添加点击事件

            $(document).ready(function () {
                $('table tbody tr', id).click(function (e) {
                    if (IsProfessing) {
                        // 如果按住了Ctrl键，则切换行的选中状态
                        $(this).toggleClass('active');
                    } else {
                        // 如果没有按住Ctrl键，则单选当前行
                        $('table tbody tr', id).removeClass('active');
                        $(this).addClass('active');
                    }
                });
            });
        },

        removeallRows: function (id) {
            $('table tbody tr', id).removeClass('active');
        },

        addRows: function (id) {
            var actionItem = [];
            $('table tbody tr', id).each(function (index) {
                if ($(this).hasClass('active')) {
                    actionItem.push(index);
                }
            });
            return actionItem;
        }
    };

    win.PageUtil = obj;
    win.SelectSearch = obj2;
    win.SelectedRows = obj3;

})(window);

// console.log('PageEx loaded' + window.PageEx);

