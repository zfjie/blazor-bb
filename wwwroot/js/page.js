﻿(function (win, undefined) {

    var obj = {

        // check the browser type
        getBrowserInfo: function () {
            var agt = navigator.userAgent.toLowerCase();
            if (agt.indexOf("opera") != -1) return 'Opera';
            if (agt.indexOf("staroffice") != -1) return 'Star Office';
            if (agt.indexOf("webtv") != -1) return 'WebTV';
            if (agt.indexOf("beonex") != -1) return 'Beonex';
            if (agt.indexOf("chimera") != -1) return 'Chimera';
            if (agt.indexOf("netpositive") != -1) return 'NetPositive';
            if (agt.indexOf("phoenix") != -1) return 'Phoenix';
            if (agt.indexOf("firefox") != -1) return 'Firefox';
            if (agt.indexOf("safari") != -1) return 'Safari';
            if (agt.indexOf("skipstone") != -1) return 'SkipStone';
            if (agt.indexOf("msie") != -1) return 'Internet Explorer';
            if (agt.indexOf("netscape") != -1) return 'Netscape';
            if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
            if (agt.indexOf('\/') != -1) {
                if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
                    return navigator.userAgent.substr(0, agt.indexOf('\/'));
                } else
                    return 'Netscape';
            } else if (agt.indexOf(' ') != -1)
                return navigator.userAgent.substr(0, agt.indexOf(' '));
            else
                return navigator.userAgent;
        },

        apply: function (object, config) {
            if (object && config && typeof config === 'object') {
                var i;
                for (i in config) {
                    object[i] = config[i];
                }
            }
            return object;
        },

        resolve: function (ns, data) {
            var x = this;
            var xx = x.ns(ns);
            x.apply(xx, data);
            return xx;
        },

        extend: function (config) { return PageEx.apply(PageEx, config); },

        sleep: function (timeout, afterFn) {
            var timerId = setTimeout(function () {
                if (afterFn) afterFn();
                clearTimeout(timerId);
            }, timeout);
        },

        anyModals: function () {

            return $(".modal:visible").length;

        },

        showModal: function (selector, autoHide) {

            if (autoHide) {
                $(selector).modal({ backdrop: true, keyboard: true });
                $(selector).on('hidden.bs.modal', function (e) {
                    $(selector).modal('dispose');
                })
            }
            else {
                $(selector).modal({ backdrop: 'static', keyboard: false });
            }

        },

        hideModal: function (selector) {

            var timerId = setTimeout(function () {

                $(selector).modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                clearTimeout(timerId);

            }, 200);

        }

    };

    win.PageEx = obj;

    win.getObject = function (name) {
        return window[name];
    }

    win.addScriptTag = function loadJs(sourceUrl) {
        if (sourceUrl.Length == 0) {
            console.error("Invalid source URL");
            return;
        }
        var tag = document.createElement('script');
        tag.src = sourceUrl;
        tag.type = "text/javascript";
        tag.onload = function () {
            console.log("Script loaded successfully");
        }
        tag.onerror = function () {
            console.error("Failed to load script");
        }
        document.body.appendChild(tag);
    }

})(window);

// console.log('PageEx loaded' + window.PageEx);

window.downloadFileFromStream = async (fileName, contentStreamReference) => {
    const arrayBuffer = await contentStreamReference.arrayBuffer();
    const blob = new Blob([arrayBuffer]);
    const url = URL.createObjectURL(blob);
    const anchorElement = document.createElement('a');
    anchorElement.href = url;
    anchorElement.download = fileName ?? '';
    anchorElement.click();
    anchorElement.remove();
    URL.revokeObjectURL(url);
}